<!-- 
    SPDX-FileCopyrightText: (c) 2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: CC0-1.0 
-->


# xmlrpcpp

This module is a copy of the XmlRpc++ 0.7 library from [https://sourceforge.net/projects/xmlrpcpp/]() .

The library has a very small footprint, can be linked in as static library and does not have any dependencies.
  
The original project stopped in 2003. This copy contains fixes to make it compile under GNU/Linux (`grep` for the 
`@RRA` tag in the source code and `makefile`). MS Windows related files are omitted. 

[<img src="./img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)

### License

The source code is Copyright (c) 2002-2003 by Chris Morley. This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either [version 2.1 ](https://www.gnu.org/licenses/lgpl-2.1.html]) of the License, or (at your option) any later version. 
